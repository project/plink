name = plink_menus
description = This is not a theme. Please move to sites/all/skins. Default menu styling for plink
version = 7.x
core = 7.x
package = plink

; Menu styles
skinr[menu_styles][title] = Menu styles
skinr[menu_styles][type] = select
skinr[menu_styles][features][] = block
skinr[menu_styles][features][] = panels_pane
skinr[menu_styles][features][] = views_view
skinr[menu_styles][description] = select from available menu styles
skinr[menu_styles][options][1][label] = One level links with separators
skinr[menu_styles][options][1][class] = inline-links
skinr[menu_styles][options][2][label] = Links as columns with sub-options below
skinr[menu_styles][options][2][class] = column-links clearfix